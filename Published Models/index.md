---
date: "2020-11-20T11:31:00+01:00"
lastmod: "2020-11-24T13:28:00+01:00"

summary: "Original models from publications or models that reproduce a published result."

menu:
  Published Models:
    weight: -10
weight: 30
---
{{< figure src="featured.png" alt="Schematic image of a microscope" caption="[CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) [Font Awesome](https://fontawesome.com/license)" width="30%" >}}

Published models may be contributed by anyone to the model repository. The original authors and citation details are given in the model's description tag.

The model file may be the original which was used to generate a published result or may be a **model that reproduces a published result** in Morpheus when the original result was obtained with a different software.