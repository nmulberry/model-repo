---
MorpheusModelID: M7681

authors: [M. Kücken, L. Brusch]
contributors: [M. Kücken]

tags: [DOI:10.1016/j.devcel.2019.10.022]

title: Planarian Regeneration (Wild Type)
date: "2019-11-18T00:00:00+01:00"
lastmod: "2020-12-15T23:46:00+01:00"
---

>Spatio-temporal model of planar polarity regulation during planarian regeneration

## Introduction

This simulation reproduces Figure 4C of the [referenced paper](#reference). Parameter values correspond to the wild type pattern (control) as given in Table S2 of the referenced paper.

## Description

The model explores the establishment and stability of the wildtype polarity pattern. Initial conditions for $\vec A$ and $\vec M$ fields are chosen as arbitrarily perturbed to demonstrate stability of the wild type pattern. $(A_x, A_y)$ represents the A/P polarization system, $(M_x, M_y)$ represents the M/L polarization system and $(P_x, P_y)$ the resulting superimposed and normalized polarization. $(P_x, P_y)$ is compared to the measured polarity data of ciliary rootlets.

![Movie of the simulation](sim-wild-type.mp4)

## Reference

This model is described in the peer-reviewed publication:

>H. T.-K. Vu, S. Mansour, M. Kücken, C. Blasse, C. Basquin, J. Azimzadeh, E. W. Myers, L. Brusch, J. C. Rink: [Dynamic Polarization of the Multiciliated Planarian Epidermis between Body Plan Landmarks][reference]. *Dev. Cell* **51** (4): 526-542.e6, 2019.

[reference]: https://doi.org/10.1016/j.devcel.2019.10.022