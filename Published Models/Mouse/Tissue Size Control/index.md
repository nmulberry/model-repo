---
MorpheusModelID: M7990

authors: [L. Brusch]
contributors: [L. Brusch]

title: Tissue Size Control
date: "2020-05-25T10:42:00+02:00"
lastmod: "2020-11-16T21:10:00+01:00"
---

>Bile Pressure Drives YAP Signaling During Liver Regeneration

## Introduction

During development and regeneration, tissues must grow to the proper size. But tissue growth is just the result of independent cell cycle control systems in the individual cells. How does each cell sense the state of the tissue as a whole?

This fundamental problem has been studied for tissue growth during liver regeneration in mice after partial hepatectomy (removal of a large part of the organ).

This model considers the metabolic (bile) overload and following mechano-sensory mechanism that activated nuclear YAP to drive cell proliferation in response to the organ state. The study is published in the open access journal [Molecular Systems Biology](#reference). See also the appendix for details on the model.

![](YAP_Signaling_Pathway.png "YAP signaling pathway as considered in the model. See [linked publication](#reference) for details.")

## Description

The simulation is visualised in this movie:

![Movie visualising the simulation](YAP_Signaling_Liver_Regeneration.mp4)

## Reference

This model is described in the peer-reviewed publication:

>K. Meyer, H. Morales‐Navarrete, S. Seifert, M. Wilsch‐Braeuninger, U. Dahmen, E. M. Tanaka, L. Brusch, Y. Kalaidzidis, M.  Zerial: [Bile canaliculi remodeling activates YAP via the actin cytoskeleton during liver regeneration][reference]. *Mol. Syst. Biol.* **16** (2), e8985, 2020.

[reference]: https://doi.org/10.15252/msb.20198985