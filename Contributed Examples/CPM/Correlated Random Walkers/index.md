---
MorpheusModelID: M2000

authors: [L. Edelstein-Keshet]
contributors: [D. Jahn]

title: Correlated Random Walkers
date: "2021-02-18T00:09:00+01:00"
lastmod: "2021-02-23T16:13:00+01:00"

tags:
- Actin
- Arp2/3 Complex
- Branching
- CellDivision
- DOI:10.1007/s00249-003-0300-4

#categories:
#- Book Title
---

>Particles moving left and right (as well as up) to mimic actin filament barbed ends

## Introduction

Particles are moving left and right (as well as up) to mimic actin filament barbed ends as motivated by the [referenced paper](#reference).

![Movie of the simulation](movie.mp4)

## Description

Initialize with a few close to $y = 0$, assign them random left or right directions.

Assymetric `CellDivision` is used to create branches at some constant probability. One daughter goes left, one goes right.

This simulation mimics the ‘unlimited Arp2/3 scenario’ where branching is not limited, and where branching takes place only close to the filament barbed ends.

![](plot_00800.png "Snapshot of the actual locations of the barbed ends with the ‘branch order’ (i.e. number of divisions) that they belong to assigned to each of them.")

The trajectory of the particles represents the actin filaments. The cell $x$ coordinate modulo the size of the $x$ range is also kept, so that the cell trajectories ‘wrap around’ (so we see ‘the filaments’ of ends that wandered off the edge of the domain).

![](logger_plot_xc_cell.center.y_time_01400.png "‘Trails’ left by the particles, i.e. the branched structure of the actin filaments.")

## Reference

>H. P. Grimm, A. B. Verkhovsky, A. Mogilner, J.-J. Meister: [Analysis of actin dynamics at the leading edge of crawling cells: implications for the shape of keratocyte lamellipodia][reference]. *Eur. Biophys. J.* **32**: 563–577, 2003.

[reference]: https://doi.org/10.1007/s00249-003-0300-4