---
MorpheusModelID: M0003

authors: [J. E. Ferrell Jr., T. Y. Tsai, Q. Yang]

title: "Delay Differential Equations: Cell Cycle"
date: "2019-10-29T16:57:00+01:00"
lastmod: "2020-10-30T12:20:00+01:00"

aliases: [/examples/delay-differential-equations-cell-cycle/]

menu:
  Built-in Examples:
    parent: ODE
    weight: 30
weight: 40
---

## Introduction

This model is a two-species version of the *Xenopus* embryonic cell cycle [shown above](/examples/ordinary-differential-equations/ode-model-cell-cycle/) that uses delay differential equations ([Ferrell *et al.*, 2011][ferrell-2011]). It exhibits sustained limit cycle oscillations.

![](cellcycledelay.png "Time plots of ODE model of *Xenopus* embryonic cell cycle, modeled with delay differential equations.")

## Description

This model uses two ```Properties``` ($\mathrm{CDK1}$ and $\mathrm{APC}$) and two ```DelayProperties``` ($\text{CDK1}_d$ and $\text{APC}_d$) with delay $\tau$. The latter are properties that return the value that has been assigned at time $t-\tau$.

The updated values of $\mathrm{CDK1}$ and $\mathrm{APC}$ are assigned to (the back of) $\text{CDK1}_d$ and $\text{APC}_d$ using ```Equations```. When these properties used in the ```DiffEqn```, they return the value assigned in the past.

The two variables are logged and both a time plot and a phase plot are drawn.

## Things to try

- Explore the effect of delays by altering the ```DelayProperty/delay```.

## Reference

>J. E. Ferrell Jr., T. Y. Tsai, Q. Yang: [Modeling the Cell Cycle: Why Do Certain Circuits Oscillate?][ferrell-2011]. *Cell* **144** (6): 874-885, 2011.

[ferrell-2011]: http://dx.doi.org/10.1016/j.cell.2011.03.006