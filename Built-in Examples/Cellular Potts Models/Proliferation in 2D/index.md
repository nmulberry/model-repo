---
MorpheusModelID: M0022

title: "Proliferation in Two Dimensions"
date: "2019-11-06T16:19:00+01:00"
lastmod: "2020-10-30T12:38:00+01:00"

aliases: [/examples/proliferation-in-two-dimensions/]

menu:
  Built-in Examples:
    parent: Cellular Potts Models
    weight: 20
weight: 120
---

## Introduction

This model shows a simulation of a growing cell population, using the cellular Potts model.

![](proliferation-2d.png "Growing cell population.")

## Description

The model specifies ```CellType``` which has a ```VolumeConstraint``` and a ```Proliferation``` plugin.  

In the ```Proliferation``` plugin, the '```Conditions``` for a cell to divide are given. Here, each cell that has more than 90% of the target volume has a small probability to divide. Once a division has taken place, the ```Equation``` defined in the ```Triggers``` elements are triggered.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/51689598?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>